package com.seiwert.server.application.atrier;

import java.util.Objects;

public class ClientContext {

    private String connectionToken;

    private String username;

    public ClientContext(String connectionToken, String username) {
        this.connectionToken = connectionToken;
        this.username = username;
    }

    public ClientContext(String connectionToken) {
        this.connectionToken = connectionToken;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getConnectionToken() {
        return connectionToken;
    }

    public void setConnectionToken(String connectionToken) {
        this.connectionToken = connectionToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClientContext that = (ClientContext) o;
        return Objects.equals(connectionToken, that.connectionToken) &&
                Objects.equals(username, that.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(connectionToken, username);
    }
}
