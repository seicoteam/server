package com.seiwert.server.application;

import com.seiwert.server.application.io.RequestExecutor;
import com.seiwert.server.application.io.SocketReader;
import com.seiwert.server.application.io.SocketWriter;

import java.net.Socket;

public class Connection {

    private final SocketReader socketReader;

    private final SocketWriter socketWriter;

    public Connection(Socket socket, RequestExecutor requestExecutor) {
        this.socketWriter = new SocketWriter(socket);
        this.socketReader = new SocketReader(socket, requestExecutor);

        socketReader.startListening();
    }

    public void sendMessage(String message) {
        socketWriter.sendMessage(message);
    }
}
