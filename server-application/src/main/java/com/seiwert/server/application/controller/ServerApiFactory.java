package com.seiwert.server.application.controller;

import com.seiwert.server.api.ServerApi;

public interface ServerApiFactory {

    ServerApi getServerApi(String name);
}
