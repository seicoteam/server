package com.seiwert.server.application.service;

import com.seiwert.server.application.entity.Campaign;
import com.seiwert.server.application.entity.SceneElement;
import com.seiwert.server.application.model.CampaignModel;
import com.seiwert.server.application.model.SceneElementModel;
import com.seiwert.server.application.repository.CampaignRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class CampaignService {

    private final CampaignRepository campaignRepository;

    public CampaignService(CampaignRepository campaignRepository) {
        this.campaignRepository = campaignRepository;
    }

    public Optional<Campaign> findById(String id) {
        return campaignRepository.findById(id).map(this::convertModelToEntity);
    }

    public Campaign save(Campaign campaign) {
        return convertModelToEntity(campaignRepository.save(convertEntityToModel(campaign)));
    }

    private Campaign convertModelToEntity(CampaignModel campaignModel) {
        if (campaignModel == null) {
            return null;
        }

        List<SceneElement> sceneElementList = campaignModel.getSceneElements()
                .stream()
                .map(this::convertSceneElementModelToEntity).collect(Collectors.toList());

        return new Campaign(campaignModel.getId(),
                campaignModel.getName(),
                campaignModel.getGmId(),
                campaignModel.getPlayerIdList(),
                sceneElementList);
    }

    private CampaignModel convertEntityToModel(Campaign campaign) {
        if (campaign == null) {
            return null;
        }

        List<SceneElementModel> sceneElementModelList = campaign.sceneElementsById
                .values()
                .stream()
                .map(this::convertSceneElementEntityToModel).collect(Collectors.toList());

        return new CampaignModel(campaign.id,
                campaign.name,
                campaign.gmId,
                campaign.playerIdList,
                sceneElementModelList);
    }

    private SceneElement convertSceneElementModelToEntity(SceneElementModel sceneElementModel) {
        if (sceneElementModel == null) {
            return null;
        }

        return new SceneElement(sceneElementModel.getId(),
                sceneElementModel.getPosition(),
                sceneElementModel.getSize());
    }

    private SceneElementModel convertSceneElementEntityToModel(SceneElement sceneElement) {
        if (sceneElement == null) {
            return null;
        }

        return new SceneElementModel(sceneElement.id,
                sceneElement.position,
                sceneElement.size);
    }
}
