package com.seiwert.server.application.repository;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

@NoRepositoryBean
public interface CommonPagingRepository<MODELE_TYPE> extends PagingAndSortingRepository<MODELE_TYPE, String>, JpaSpecificationExecutor<MODELE_TYPE> {
}