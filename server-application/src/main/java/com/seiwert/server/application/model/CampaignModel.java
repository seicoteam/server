package com.seiwert.server.application.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "CAMPAIGN")
public class CampaignModel {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(name = "ID")
    private String id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "GM_ID")
    private String gmId;

    @ElementCollection(fetch = FetchType.LAZY)
    @CollectionTable(name = "CAMPAIGN_PLAYER", joinColumns = @JoinColumn(name = "CAMPAIGN_ID"))
    @Column(name = "PLAYER_ID")
    private List<String> playerIdList;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "CAMPAIGN_ID")
    private List<SceneElementModel> sceneElements;

    public CampaignModel() {
    }

    public CampaignModel(String id, String name, String gmId, List<String> playerIdList, List<SceneElementModel> sceneElements) {
        this.id = id;
        this.name = name;
        this.gmId = gmId;
        this.playerIdList = playerIdList;
        this.sceneElements = sceneElements;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGmId() {
        return gmId;
    }

    public void setGmId(String gmId) {
        this.gmId = gmId;
    }

    public List<String> getPlayerIdList() {
        return playerIdList;
    }

    public void setPlayerIdList(List<String> playerIdList) {
        this.playerIdList = playerIdList;
    }

    public List<SceneElementModel> getSceneElements() {
        return sceneElements;
    }

    public void setSceneElements(List<SceneElementModel> sceneElements) {
        this.sceneElements = sceneElements;
    }
}
