package com.seiwert.server.application.atrier;

import java.util.*;

public final class CampaignManager {

    private static final Map<String, String> CAMPAIGN_ID_BY_PLAYER_CONNECTED_ID = new HashMap<>();

    private static final Map<String, List<String>> PLAYER_CONNECTED_IDS_BY_CAMPAIGN_ID = new HashMap<>();

    private CampaignManager () {
    }

    public static void addPlayerToCampaign(String playerId, String campaignId) {
        CAMPAIGN_ID_BY_PLAYER_CONNECTED_ID.put(playerId, campaignId);

        if (!PLAYER_CONNECTED_IDS_BY_CAMPAIGN_ID.containsKey(campaignId)) {
            PLAYER_CONNECTED_IDS_BY_CAMPAIGN_ID.put(campaignId, new ArrayList<>());
        }

        if (!PLAYER_CONNECTED_IDS_BY_CAMPAIGN_ID.get(campaignId).contains(playerId)) {
            PLAYER_CONNECTED_IDS_BY_CAMPAIGN_ID.get(campaignId).add(playerId);
        }
    }

    public static void removePlayerFromCampaign(String playerId) {
        String campaignId = CAMPAIGN_ID_BY_PLAYER_CONNECTED_ID.remove(playerId);

        PLAYER_CONNECTED_IDS_BY_CAMPAIGN_ID.get(campaignId).remove(playerId);
    }

    public static Optional<String> getCurrentCampaignOfPlayer(String PlayerId) {
        return Optional.ofNullable(CAMPAIGN_ID_BY_PLAYER_CONNECTED_ID.getOrDefault(PlayerId, null));
    }

    public static List<String> getCurrentPlayerIdsOfCampaign(String campaignId) {
        return PLAYER_CONNECTED_IDS_BY_CAMPAIGN_ID.getOrDefault(campaignId, new ArrayList<>());
    }
}
