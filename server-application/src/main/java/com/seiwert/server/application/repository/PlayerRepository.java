package com.seiwert.server.application.repository;

import com.seiwert.server.application.model.PlayerModel;

import java.util.List;
import java.util.Optional;

public interface PlayerRepository extends CommonPagingRepository<PlayerModel> {

    List<PlayerModel> findByIdIn(List<String> ids);

    Optional<PlayerModel> findByName(String name);
}
