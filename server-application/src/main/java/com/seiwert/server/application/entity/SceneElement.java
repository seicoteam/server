package com.seiwert.server.application.entity;

import com.seiwert.common.Position;
import com.seiwert.common.Size;

public class SceneElement {

    public final String id;

    public Position position;

    public Size size;

    public SceneElement(String id, Position position, Size size) {
        this.id = id;
        this.position = position;
        this.size = size;
    }
}
