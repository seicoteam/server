package com.seiwert.server.application.service;

import com.seiwert.client.api.dto.CampaignApi;
import com.seiwert.common.Position;
import com.seiwert.common.Size;
import com.seiwert.server.application.CallerProvider;
import com.seiwert.server.application.ConnectionManager;
import com.seiwert.server.application.atrier.CampaignManager;
import com.seiwert.server.application.atrier.ClientContext;
import com.seiwert.server.application.entity.Campaign;
import com.seiwert.server.application.entity.Player;
import com.seiwert.server.application.entity.SceneElement;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ResizeElementService {

    private final CampaignService campaignService;

    private final PlayerService playerService;

    public ResizeElementService(CampaignService campaignService, PlayerService playerService) {
        this.campaignService = campaignService;
        this.playerService = playerService;
    }

    public void resizeElement(ClientContext callerContext, String elementId, Size newSize) {
        Optional<Player> playerOptional = playerService.findByName(callerContext.getUsername());

        Optional<String> campaignIdOptional = CampaignManager.getCurrentCampaignOfPlayer(playerOptional.get().id);

        if (campaignIdOptional.isPresent()) {
            String campaignId = campaignIdOptional.get();

            Optional<Campaign> campaignOptional = campaignService.findById(campaignId);

            if (campaignOptional.isPresent()) {
                Campaign campaign = campaignOptional.get();

                SceneElement sceneElement = campaign.sceneElementsById.get(elementId);

                sceneElement.size = newSize;

                campaignService.save(campaign);

                sendElementResizedToCampaignPlayers(callerContext, elementId, newSize, campaignId);
            }
        }
    }

    private void sendElementResizedToCampaignPlayers(ClientContext callerContext, String elementId, Size newSize, String campaignId) {
        List<String> playerConnectedInCampaignIds = CampaignManager.getCurrentPlayerIdsOfCampaign(campaignId);

        List<String> playerConnectedInCampaignUsernames = playerService.findAll(playerConnectedInCampaignIds)
                .stream().map(player -> player.name)
                .collect(Collectors.toList());

        List<ClientContext> clientContexts = playerConnectedInCampaignUsernames.stream()
                .map(username -> new ClientContext(ConnectionManager.getTokenByConnection(ConnectionManager.getConnectionByUsername(username)), username))
                .collect(Collectors.toList());

        List<ClientContext> clientContextListWithoutCaller =  clientContexts.stream()
                .filter(clientContext -> !callerContext.equals(clientContext))
                .collect(Collectors.toList());

        CampaignApi campaignCallerForAllButCaller = CallerProvider.getCallerForAll(clientContextListWithoutCaller, CampaignApi.class);

        campaignCallerForAllButCaller.resizeElement(elementId, newSize);
    }
}
