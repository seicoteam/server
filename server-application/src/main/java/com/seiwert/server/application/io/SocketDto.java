package com.seiwert.server.application.io;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SocketDto {

    public final String serviceName;

    public final String methodeName;

    public final Object[] args;

    @JsonCreator
    public SocketDto(@JsonProperty("serviceName") String serviceName,
                     @JsonProperty("methodeName") String methodeName,
                     @JsonProperty("args") Object[] args) {
        this.serviceName = serviceName;
        this.methodeName = methodeName;
        this.args = args;
    }
}
