package com.seiwert.server.application.repository;

import com.seiwert.server.application.model.CampaignModel;

public interface CampaignRepository extends CommonPagingRepository<CampaignModel>{
}
