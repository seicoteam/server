package com.seiwert.server.application.controller;

import com.seiwert.server.api.LoginApi;
import com.seiwert.server.api.dto.ClientContextDto;
import com.seiwert.server.application.atrier.ClientContext;
import com.seiwert.server.application.service.LoginService;

import org.springframework.stereotype.Component;

@Component
public class LoginController implements LoginApi {

    private final LoginService loginService;

    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    @Override
    public void login(ClientContextDto currentClientContextDto, String usernameToLogin) {
        loginService.log(new ClientContext(currentClientContextDto.connectionToken), usernameToLogin);
    }
}
