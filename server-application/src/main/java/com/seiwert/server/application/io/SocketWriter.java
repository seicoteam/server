package com.seiwert.server.application.io;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public final class SocketWriter {

    private PrintWriter writer = null;

    public SocketWriter(Socket socket) {
        try {
            writer = new PrintWriter(socket.getOutputStream(), true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendMessage(String message) {
        writer.write(message);
        writer.flush();
    }
}
