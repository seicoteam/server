package com.seiwert.server.application.service;

import com.seiwert.client.api.dto.CampaignApi;
import com.seiwert.common.Position;
import com.seiwert.common.Size;
import com.seiwert.server.application.CallerProvider;
import com.seiwert.server.application.ConnectionManager;
import com.seiwert.server.application.atrier.CampaignManager;
import com.seiwert.server.application.atrier.ClientContext;
import com.seiwert.server.application.entity.Campaign;
import com.seiwert.server.application.entity.Player;
import com.seiwert.server.application.entity.SceneElement;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class AddElementService {

    private final CampaignService campaignService;

    private final PlayerService playerService;

    public AddElementService(CampaignService campaignService, PlayerService playerService) {
        this.campaignService = campaignService;
        this.playerService = playerService;
    }

    public void addElement(ClientContext callerContext, Position position, Size size) {
        Optional<Player> playerOptional = playerService.findByName(callerContext.getUsername());

        Optional<String> campaignIdOptional = CampaignManager.getCurrentCampaignOfPlayer(playerOptional.get().id);

        if (campaignIdOptional.isPresent()) {
            String campaignId = campaignIdOptional.get();

            Optional<Campaign> campaignOptional = campaignService.findById(campaignId);

            if (campaignOptional.isPresent()) {
                Campaign campaign = campaignOptional.get();

                Set<String> presentSceneElementIds = campaign.sceneElementsById.keySet();

                SceneElement newSceneElement = new SceneElement(null,
                                                             position,
                                                             size);

                campaign.addSceneElement(newSceneElement);

                Campaign campaignSaved = campaignService.save(campaign);

                Set<String> sceneElementIds = campaignSaved.sceneElementsById.keySet();

                String newSceneElementId = sceneElementIds.stream()
                        .filter(sceneElementId -> !presentSceneElementIds.contains(sceneElementId))
                        .findFirst()
                        .orElse(null);

                SceneElement sceneElementSaved = campaignSaved.sceneElementsById.get(newSceneElementId);

                sendElementAddedToCampaignPlayers(sceneElementSaved, campaignId);
            }
        }
    }

    private void sendElementAddedToCampaignPlayers(SceneElement newSceneElement, String campaignId) {
        List<String> playerConnectedInCampaignIds = CampaignManager.getCurrentPlayerIdsOfCampaign(campaignId);

        List<String> playerConnectedInCampaignUsernames = playerService.findAll(playerConnectedInCampaignIds)
                .stream().map(player -> player.name)
                .collect(Collectors.toList());

        List<ClientContext> clientContexts = playerConnectedInCampaignUsernames.stream()
                .map(username -> new ClientContext(ConnectionManager.getTokenByConnection(ConnectionManager.getConnectionByUsername(username)), username))
                .collect(Collectors.toList());

        CampaignApi campaignCallerForAll = CallerProvider.getCallerForAll(clientContexts, CampaignApi.class);

        campaignCallerForAll.addElement(newSceneElement.id, newSceneElement.position, newSceneElement.size);
    }
}
