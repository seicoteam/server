package com.seiwert.server.application.io;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.seiwert.server.api.ServerApi;
import com.seiwert.server.application.controller.ServerApiFactory;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

@Service
public class RequestExecutor {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private final ServerApiFactory serverApiFactory;

    public RequestExecutor(ServerApiFactory serverApiFactory) {
        this.serverApiFactory = serverApiFactory;
    }

    void execute(SocketDto clientRequest) {
        String serviceName = clientRequest.serviceName;

        String controllerFullClassName = serviceName + "Controller";

        ServerApi controller = serverApiFactory.getServerApi(controllerFullClassName);

        if (controller != null) {
            Method[] methodArray = controller.getClass().getMethods();
            Method methodToInvoke = Arrays.stream(methodArray).filter(method -> method.getName().equals(clientRequest.methodeName)).findFirst().orElseGet(null);

            if (methodToInvoke != null) {
                try {
                    Object[] castedObjectList = new Object[clientRequest.args.length];
                    for (int i = 0; i < clientRequest.args.length; i++) {
                        castedObjectList[i] = MAPPER.convertValue(clientRequest.args[i], methodToInvoke.getParameterTypes()[i]);
                    }

                    methodToInvoke.invoke(controller, castedObjectList);
                } catch (IllegalAccessException | InvocationTargetException e) {
                    e.printStackTrace();
                }
            } else {
                System.err.println("Method " + clientRequest.methodeName + " not handled for controller " + serviceName);
            }
        } else {
            System.err.println("Controller " + controllerFullClassName + " not found");
        }
    }
}
