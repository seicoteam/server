package com.seiwert.server.application.service;

import com.seiwert.server.application.entity.Player;
import com.seiwert.server.application.model.PlayerModel;
import com.seiwert.server.application.repository.PlayerRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class PlayerService {

    private final PlayerRepository playerRepository;

    public PlayerService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public List<Player> findAll(List<String> playerConnectedInCampaignIds) {
        return playerRepository.findByIdIn(playerConnectedInCampaignIds).stream()
                .map(this::convertModelToEntity)
                .collect(Collectors.toList());
    }

    public Optional<Player> findByName(String name) {
        return playerRepository.findByName(name).map(this::convertModelToEntity);
    }

    private Player convertModelToEntity(PlayerModel playerModel) {
        if (playerModel == null) {
            return null;
        }

        return new Player(playerModel.getId(),
                playerModel.getName());
    }
}
