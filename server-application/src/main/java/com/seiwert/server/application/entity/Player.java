package com.seiwert.server.application.entity;

public class Player {

    public final String id;

    public final String name;

    public Player(String id, String name) {
        this.id = id;
        this.name = name;
    }
}
