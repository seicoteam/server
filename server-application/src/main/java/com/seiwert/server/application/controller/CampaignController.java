package com.seiwert.server.application.controller;

import com.seiwert.common.Position;
import com.seiwert.common.Size;
import com.seiwert.server.api.CampaignApi;
import com.seiwert.server.api.dto.ClientContextDto;
import com.seiwert.server.application.atrier.ClientContext;
import com.seiwert.server.application.service.*;
import org.springframework.stereotype.Component;

@Component
public class CampaignController implements CampaignApi {

    private final JoinCampaignService joinCampaignService;

    private final MoveElementService moveElementService;

    private final AddElementService addElementService;

    private final RemoveElementService removeElementService;

    private final ResizeElementService resizeElementService;

    public CampaignController(JoinCampaignService joinCampaignService,
                              MoveElementService moveElementService,
                              AddElementService addElementService,
                              RemoveElementService removeElementService,
                              ResizeElementService resizeElementService) {
        this.joinCampaignService = joinCampaignService;
        this.moveElementService = moveElementService;
        this.addElementService = addElementService;
        this.removeElementService = removeElementService;
        this.resizeElementService = resizeElementService;
    }

    @Override
    public void joinCampaign(ClientContextDto clientContextDto) {
        joinCampaignService.makePlayerJoinCampaign(new ClientContext(clientContextDto.connectionToken, clientContextDto.username));
    }

    @Override
    public void moveElement(ClientContextDto clientContextDto, String elementId, Position newPosition) {
        moveElementService.moveElement(new ClientContext(clientContextDto.connectionToken, clientContextDto.username), elementId, newPosition);
    }

    @Override
    public void addElement(ClientContextDto clientContextDto, Position position, Size size) {
        addElementService.addElement(new ClientContext(clientContextDto.connectionToken, clientContextDto.username), position, size);
    }

    @Override
    public void removeElement(ClientContextDto clientContextDto, String elementId) {
        removeElementService.removeElement(new ClientContext(clientContextDto.connectionToken, clientContextDto.username), elementId);
    }

    @Override
    public void resizeElement(ClientContextDto clientContextDto, String elementId, Size size) {
        resizeElementService.resizeElement(new ClientContext(clientContextDto.connectionToken, clientContextDto.username), elementId, size);
    }
}
