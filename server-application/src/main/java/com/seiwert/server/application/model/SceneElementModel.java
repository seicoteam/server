package com.seiwert.server.application.model;

import com.seiwert.common.Position;
import com.seiwert.common.Size;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "SCENE_ELEMENT")
public class SceneElementModel {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid", strategy = "uuid")
    @Column(name = "ID")
    private String id;

    @Column(name = "POSITION_X")
    private double positionX;

    @Column(name = "POSITION_Y")
    private double positionY;

    @Column(name = "WIDTH")
    private double width;

    @Column(name = "HEIGHT")
    private double height;

    public SceneElementModel() {
    }

    public SceneElementModel(String id, double positionX, double positionY, double width, double height) {
        this.id = id;
        this.positionX = positionX;
        this.positionY = positionY;
        this.width = width;
        this.height = height;
    }

    public SceneElementModel(String id, Position position, Size size) {
        this.id = id;
        this.positionX = position.x;
        this.positionY = position.y;
        this.width = size.width;
        this.height = size.height;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getPositionX() {
        return positionX;
    }

    public void setPositionX(double positionX) {
        this.positionX = positionX;
    }

    public double getPositionY() {
        return positionY;
    }

    public void setPositionY(double positionY) {
        this.positionY = positionY;
    }

    public Position getPosition() {
        return new Position(positionX, positionY);
    }

    public void setPosition(double positionX, double positionY) {
        this.positionX = positionX;
        this.positionY = positionY;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public Size getSize() {
        return new Size(width, height);
    }

    public void setSize(Size size) {
        this.width = size.width;
        this.height = size.height;
    }
}
