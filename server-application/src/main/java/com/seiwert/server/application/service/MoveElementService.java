package com.seiwert.server.application.service;

import com.seiwert.client.api.dto.CampaignApi;
import com.seiwert.common.Position;
import com.seiwert.server.application.CallerProvider;
import com.seiwert.server.application.ConnectionManager;
import com.seiwert.server.application.atrier.CampaignManager;
import com.seiwert.server.application.atrier.ClientContext;
import com.seiwert.server.application.entity.Campaign;
import com.seiwert.server.application.entity.Player;
import com.seiwert.server.application.entity.SceneElement;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MoveElementService {

    private final CampaignService campaignService;

    private final PlayerService playerService;

    public MoveElementService(CampaignService campaignService, PlayerService playerService) {
        this.campaignService = campaignService;
        this.playerService = playerService;
    }

    public void moveElement(ClientContext callerContext, String elementId, Position newPosition) {
        Optional<Player> playerOptional = playerService.findByName(callerContext.getUsername());

        Optional<String> campaignIdOptional = CampaignManager.getCurrentCampaignOfPlayer(playerOptional.get().id);

        if (campaignIdOptional.isPresent()) {
            String campaignId = campaignIdOptional.get();

            Optional<Campaign> campaignOptional = campaignService.findById(campaignId);

            if (campaignOptional.isPresent()) {
                Campaign campaign = campaignOptional.get();

                SceneElement sceneElement = campaign.sceneElementsById.get(elementId);

                sceneElement.position = newPosition;

                campaignService.save(campaign);

                sendElementMovedToCampaignPlayers(callerContext, elementId, newPosition, campaignId);
            }
        }
    }

    private void sendElementMovedToCampaignPlayers(ClientContext callerContext, String elementId, Position newPosition, String campaignId) {
        List<String> playerConnectedInCampaignIds = CampaignManager.getCurrentPlayerIdsOfCampaign(campaignId);

        List<String> playerConnectedInCampaignUsernames = playerService.findAll(playerConnectedInCampaignIds)
                .stream().map(player -> player.name)
                .collect(Collectors.toList());

        List<ClientContext> clientContexts = playerConnectedInCampaignUsernames.stream()
                .map(username -> new ClientContext(ConnectionManager.getTokenByConnection(ConnectionManager.getConnectionByUsername(username)), username))
                .collect(Collectors.toList());

        List<ClientContext> clientContextListWithoutCaller =  clientContexts.stream()
                .filter(clientContext -> !callerContext.equals(clientContext))
                .collect(Collectors.toList());

        CampaignApi campaignCallerForAll = CallerProvider.getCallerForAll(clientContextListWithoutCaller, CampaignApi.class);

        campaignCallerForAll.moveElement(elementId, newPosition);
    }
}
