package com.seiwert.server.application;

import org.flywaydb.core.Flyway;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerApplication {

    public static void main(String[] args) {
        Flyway flyway = Flyway.configure().dataSource("jdbc:mysql://localhost:3306/db_server?zeroDateTimeBehavior=convertToNull&serverTimezone=UTC",
                "SERVER",
                "").load();
        flyway.clean();
        flyway.migrate();

        SpringApplication.run(ServerApplication.class, args);
    }
}
