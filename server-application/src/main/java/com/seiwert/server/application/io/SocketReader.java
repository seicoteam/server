package com.seiwert.server.application.io;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.seiwert.server.application.ConnectionManager;
import org.apache.log4j.Logger;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.Socket;

public class SocketReader {

    private static final Logger LOGGER = Logger.getLogger(SocketReader.class);

    private static final ObjectMapper MAPPER = new ObjectMapper();

    private final RequestExecutor requestExecutor;

    private BufferedInputStream reader = null;

    private boolean listening = false;

    private Socket socket;

    public SocketReader(Socket socket, RequestExecutor requestExecutor) {
        this.requestExecutor = requestExecutor;
        this.socket = socket;

        try {
            reader = new BufferedInputStream(socket.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void startListening() {
        listening = true;

        new Thread(this::listen).start();
    }

    public void stopListening() {
        listening = false;
    }

    private void listen() {
        while(listening) {
            try {
                String clientMessage = read();

                if (clientMessage != null) {
                    LOGGER.debug("Message from client :" + clientMessage);

                    SocketDto clientRequest = MAPPER.readValue(clientMessage, SocketDto.class);
                    requestExecutor.execute(clientRequest);
                } else {
                    stopListening();

                    ConnectionManager.handleSocketDisconnection(socket);

                    reader.close();
                }
            } catch (IOException e) {
                stopListening();

                ConnectionManager.handleSocketDisconnection(socket);
            }
        }
    }

    private String read() throws IOException{
        byte[] b = new byte[4096];
        int stream = reader.read(b);
        if (stream != -1) {
            return new String(b, 0, stream);
        }

        return null;
    }
}
