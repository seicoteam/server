package com.seiwert.server.application.entity;

import java.util.*;

public class Campaign {

    public final String id;

    public final String name;

    public final String gmId;

    public final List<String> playerIdList;

    public final Map<String, SceneElement> sceneElementsById;

    public Campaign(String id, String name, String gmId, List<String> playerIdList, List<SceneElement> sceneElementList) {
        this.id = id;
        this.name = name;
        this.gmId = gmId;
        this.playerIdList = playerIdList;
        this.sceneElementsById = new HashMap<>();

        sceneElementList.forEach(sceneElement -> sceneElementsById.put(sceneElement.id, sceneElement));
    }

    public List<SceneElement> getSceneElements() {
        List<SceneElement> sceneElements = new ArrayList<>();

        sceneElementsById.forEach((elementId, sceneElement) -> sceneElements.add(sceneElement));

        return Collections.unmodifiableList(sceneElements);
    }

    public void addSceneElement(SceneElement sceneElement) {
        sceneElementsById.put(sceneElement.id, sceneElement);
    }

    public void removeSceneElement(SceneElement sceneElementToRemove) {
        sceneElementsById.remove(sceneElementToRemove.id);
    }
}
