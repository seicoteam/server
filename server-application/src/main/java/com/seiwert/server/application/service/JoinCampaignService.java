package com.seiwert.server.application.service;

import com.seiwert.client.api.dto.CampaignApi;
import com.seiwert.client.api.dto.SceneDto;
import com.seiwert.server.application.CallerProvider;
import com.seiwert.server.application.atrier.CampaignManager;
import com.seiwert.server.application.atrier.ClientContext;
import com.seiwert.server.application.entity.Campaign;
import com.seiwert.server.application.entity.Player;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.stream.Collectors;

@Service
@Transactional
public class JoinCampaignService {

    private final static Logger LOGGER = Logger.getLogger(JoinCampaignService.class);

    private final CampaignService campaignService;

    private final PlayerService playerService;

    public JoinCampaignService(CampaignService campaignService, PlayerService playerService) {
        this.campaignService = campaignService;
        this.playerService = playerService;
    }

    public void makePlayerJoinCampaign(ClientContext clientContext) {
        //TODO : A remplacer avec gestion de multi campagnes
        Campaign campaign = campaignService.findById("00000000000000000000000000000000000000000001").get();

        Player player = playerService.findByName(clientContext.getUsername()).get();

        CampaignApi campaignCaller = CallerProvider.getCaller(clientContext, CampaignApi.class);

        SceneDto sceneDto = new SceneDto(campaign.getSceneElements().stream()
                .map(sceneElement -> new SceneDto.SceneElementDto(sceneElement.id, sceneElement.position, sceneElement.size))
                .collect(Collectors.toList()));

        if (campaign.gmId.equals(player.id)) {
            campaignCaller.initCampaignAsGm(sceneDto);

            CampaignManager.addPlayerToCampaign(player.id, campaign.id);
        } else if (campaign.playerIdList.contains(player.id)) {
            campaignCaller.initCampaignAsPlayer(sceneDto);

            CampaignManager.addPlayerToCampaign(player.id, campaign.id);
        } else {
            LOGGER.error("Player " + player.name + "does not participate to campaign : " + campaign.name);
        }
    }
}
