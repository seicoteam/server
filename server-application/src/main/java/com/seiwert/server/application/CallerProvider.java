package com.seiwert.server.application;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.seiwert.server.application.atrier.ClientContext;
import com.seiwert.server.application.io.SocketDto;

import java.lang.reflect.Proxy;
import java.util.List;

public class CallerProvider {

    private static final ObjectMapper MAPPER = new ObjectMapper();

    public static <CALLER_TYPE> CALLER_TYPE getCaller(ClientContext clientContext, Class<CALLER_TYPE> callerInterface) {
        ClassLoader cl = CallerProvider.class.getClassLoader();
        Class[] interfaces = new Class[]{ callerInterface };

        Object object = Proxy.newProxyInstance(cl, interfaces, (proxy, method, args) -> {
            callMethod(clientContext, callerInterface.getSimpleName().split("Api")[0], method.getName(), args);
            return null;
        });

        return (CALLER_TYPE) object;
    }

    private static void callMethod(ClientContext clientContext, String serviceName, String methodName, Object[] paramList) {
        String message = null;
        try {
            message = MAPPER.writeValueAsString(new SocketDto(serviceName, methodName, paramList));
            System.out.println(message);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        ConnectionManager.getConnectionByToken(clientContext.getConnectionToken()).sendMessage(message);
    }

    public static <CALLER_TYPE> CALLER_TYPE getCallerForAll(List<ClientContext> clientContexts, Class<CALLER_TYPE> callerInterface) {
        ClassLoader cl = CallerProvider.class.getClassLoader();
        Class[] interfaces = new Class[]{ callerInterface };

        Object object = Proxy.newProxyInstance(cl, interfaces, (proxy, method, args) -> {
            callMethodForAll(clientContexts, callerInterface.getSimpleName().split("Api")[0], method.getName(), args);
            return null;
        });

        return (CALLER_TYPE) object;
    }

    private static void callMethodForAll(List<ClientContext> clientContexts, String serviceName, String methodName, Object[] paramList) {
        String message = null;
        try {
            message = MAPPER.writeValueAsString(new SocketDto(serviceName, methodName, paramList));
            System.out.println(message);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        for (ClientContext clientContext : clientContexts) {
            ConnectionManager.getConnectionByToken(clientContext.getConnectionToken()).sendMessage(message);
        }
    }
}
