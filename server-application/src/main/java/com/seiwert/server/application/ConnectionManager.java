package com.seiwert.server.application;

import com.seiwert.client.api.dto.ContextApi;
import com.seiwert.server.application.atrier.ClientContext;
import com.seiwert.server.application.io.RequestExecutor;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class ConnectionManager {

    /*
     * TODO : Gérer la déconnexion du serveur
     */
    private static boolean acceptingNewConnection = true;

    private static final Map<String, Connection> connectionsByToken = new HashMap<>();
    private static final Map<Connection, String> tokensByConnection = new HashMap<>();

    private static final Map<String, Connection> connectionsByUsername = new HashMap<>();
    private static final Map<Connection, String> usernamesByConnection = new HashMap<>();

    private static final Map<Socket, Connection> connectionsBySocket = new HashMap<>();

    private static final Logger LOGGER = Logger.getLogger(ConnectionManager.class);

    private final RequestExecutor requestExecutor;

    public ConnectionManager(RequestExecutor requestExecutor) {
        this.requestExecutor = requestExecutor;

        new Thread(this::listenConnection).start();
    }

    public static Connection getConnectionByToken(String connectionToken) {
        return connectionsByToken.getOrDefault(connectionToken, null);
    }

    public static Connection getConnectionByUsername(String username) {
        return connectionsByUsername.getOrDefault(username, null);
    }

    private static Connection getConnectionBySocket(Socket socket) {
        return connectionsBySocket.getOrDefault(socket, null);
    }

    public static String getTokenByConnection(Connection connection) {
        return tokensByConnection.getOrDefault(connection, null);
    }

    public static void log(ClientContext currentClientContext, String usernameToLogin) {
        Connection connectionByToken = connectionsByToken.getOrDefault(currentClientContext.getConnectionToken(), null);
        if (connectionByToken == null) {
            LOGGER.warn("No connection found for token : " + connectionByToken);
        }

        Connection connectionByUsername = connectionsByUsername.getOrDefault(usernameToLogin, null);

        if (connectionByUsername == null) {
            connectionsByUsername.put(usernameToLogin, connectionByToken);
            usernamesByConnection.put(connectionByToken, usernameToLogin);

            ClientContext newClientContext = new ClientContext(currentClientContext.getConnectionToken(), usernameToLogin);

            ContextApi contextCaller = CallerProvider.getCaller(newClientContext, ContextApi.class);

            contextCaller.validatelogin(newClientContext.getUsername());

            LOGGER.info("User " + usernameToLogin + " logged");
        } else {
            LOGGER.warn("Client with username " + usernameToLogin + " already connected");
        }
    }

    public static void handleSocketDisconnection(Socket socket) {
        Connection connection = getConnectionBySocket(socket);

        String token = tokensByConnection.getOrDefault(connection, null);

        String userName = usernamesByConnection.getOrDefault(connection, null);

        connectionsByToken.remove(token);
        tokensByConnection.remove(connection);

        connectionsByUsername.remove(userName);
        usernamesByConnection.remove(connection);

        connectionsBySocket.remove(socket);

        LOGGER.warn("Client not properly disconnected");
    }

    private void listenConnection() {
        try {
            ServerSocket serverSocket = new ServerSocket(8001);

            while(acceptingNewConnection) {
                newConnection(serverSocket.accept());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void newConnection(Socket newSocket) {
        Connection newConnection = new Connection(newSocket, requestExecutor);

        connectionsBySocket.put(newSocket, newConnection);

        String token = UUID.randomUUID().toString();

        connectionsByToken.put(token, newConnection);
        tokensByConnection.put(newConnection, token);

        ContextApi contextCaller = CallerProvider.getCaller(new ClientContext(token), ContextApi.class);

        contextCaller.setConnectionToken(token);
    }
}
