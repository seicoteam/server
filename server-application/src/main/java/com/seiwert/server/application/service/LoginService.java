package com.seiwert.server.application.service;

import com.seiwert.server.application.ConnectionManager;
import com.seiwert.server.application.atrier.ClientContext;
import com.seiwert.server.application.model.PlayerModel;
import com.seiwert.server.application.repository.PlayerRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LoginService {

    private final PlayerRepository playerRepository;

    public LoginService(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    public void log(ClientContext currentClientContext, String usernameToLogin) {
        Optional<PlayerModel> existingUserOptional = playerRepository.findByName(usernameToLogin);

        if(!existingUserOptional.isPresent()) {
            playerRepository.save(new PlayerModel(null, usernameToLogin));
            System.out.println("User " + usernameToLogin + " created");
        }

        ConnectionManager.log(currentClientContext, usernameToLogin);
    }
}
