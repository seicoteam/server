package com.seiwert.server.api;

import com.seiwert.server.api.dto.ClientContextDto;

public interface LoginApi extends ServerApi {

    void login(ClientContextDto clientContextDto, String username);
}
