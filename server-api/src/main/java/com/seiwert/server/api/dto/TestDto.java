package com.seiwert.server.api.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TestDto {

    public final String champ1;

    public final String champ2;

    @JsonCreator
    public TestDto(@JsonProperty("champ1") String champ1,
                   @JsonProperty("champ2") String champ2) {
        this.champ1 = champ1;
        this.champ2 = champ2;
    }
}
