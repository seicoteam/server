package com.seiwert.server.api;

import com.seiwert.common.Position;
import com.seiwert.common.Size;
import com.seiwert.server.api.dto.ClientContextDto;

public interface CampaignApi extends ServerApi{

    void joinCampaign(ClientContextDto clientContextDto);

    void moveElement(ClientContextDto clientContextDto, String elementId, Position newPosition);

    void addElement(ClientContextDto clientContextDto, Position position, Size size);

    void removeElement(ClientContextDto clientContextDto, String elementId);

    void resizeElement(ClientContextDto clientContextDto, String elementId, Size size);
}
