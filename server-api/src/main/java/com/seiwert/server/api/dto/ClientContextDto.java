package com.seiwert.server.api.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ClientContextDto {

    public final String connectionToken;

    public final String username;

    @JsonCreator
    public ClientContextDto(@JsonProperty("connectionToken") String connectionToken,
                            @JsonProperty("username") String username) {
        this.connectionToken = connectionToken;
        this.username = username;
    }
}
